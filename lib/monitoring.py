"""Monitoring for working bots in bots controller telegram bot"""

import os
from time import sleep
import lib.database_work
import loguru

notify_table = lib.database_work.NotifyWork()
bots_table = lib.database_work.BotWork()


def monitoring_for_bots_working(bot):
    """Checks process launch by procname"""
    while True:
        procnames = bots_table.get_pid_and_botnames()
        for procname in procnames:
            proc = os.popen('ps ax').read()
            if proc.find(str(procname[0])) == -1:
                bots_table.update_bot_status(procname[1], 0)
                users_id = notify_table.get_users_id_by_botname(procname[1])
                down_message = "Бот '%s' в данный момент отключён.\n"\
                    "Мы постараемся это исправить в ближайшее время.\n"\
                    "Благодарим за понимание и терпение" % procname[1]

                loguru.logger.info("STOPED %s" % procname[1])

                for user_id in users_id:
                    bot.send_message(user_id[0], down_message)
        sleep(3)
