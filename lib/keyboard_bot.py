"""Keyboard for telegram bot bots controller"""

# -*- coding: utf-8 -*-

import lib.database_work
from telebot.types import ReplyKeyboardMarkup, KeyboardButton,\
    InlineKeyboardButton, InlineKeyboardMarkup


bots_table = lib.database_work.BotWork()
users_table = lib.database_work.UserWork()
notify_table = lib.database_work.NotifyWork()


class ButtonsText:
    add_bot = 'Добавить бота [⏬]'
    bots = 'Боты [🤖]'
    bots_list = 'Список [🗒]'
    bots_control = 'Управление [🛂]'
    bots_log = 'Журнал [📜]'
    bots_remove = 'Удалить [❌]'

    settings = 'Уведомления [🔊]'
    settings_notify_add = 'Добавить оповещение [ℹ️]'
    settings_notify_control = 'Управление оповещениями [🔣]'
    mailing = 'Рассылка [📭]'
    back = 'Назад [◀️]'

    inl_start = 'Запустить [▶️]'
    inl_stop = 'Остановить [⏹]'
    inl_restart = 'Перезапустить [🔄]'

    admin = 'Администрирование [⭐️]'
    admin_add_admin = 'Добавить администратора [➕]'
    admin_del_admin = 'Удалить администатора [➖]'

    user_bots = 'Мои боты [💡]'

    support = 'Поддержка [🆘]'
    support_faq = 'Вопрос - Ответ [⁉️]'
    support_write = 'Обратная связь [📝]'


# Main menu keyboard and buttons
mainmenu_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_mm_add_bot = KeyboardButton(ButtonsText.add_bot)
btn_mm_bots = KeyboardButton(ButtonsText.bots)
btn_mm_settings = KeyboardButton(ButtonsText.settings)
btn_mm_mailing = KeyboardButton(ButtonsText.mailing)
btn_mm_admin = KeyboardButton(ButtonsText.admin)

mainmenu_kb.add(btn_mm_add_bot)
mainmenu_kb.add(btn_mm_bots)
mainmenu_kb.add(btn_mm_settings)
mainmenu_kb.add(btn_mm_mailing)
mainmenu_kb.add(btn_mm_admin)

# Go back button and keyboard
back_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_back = KeyboardButton(ButtonsText.back)
back_kb.add(btn_back)

# Bots list keyboard and buttons
bots_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_bt_list = KeyboardButton(ButtonsText.bots_list)
btn_bt_control = KeyboardButton(ButtonsText.bots_control)
btn_bt_log = KeyboardButton(ButtonsText.bots_log)
btn_bt_remove = KeyboardButton(ButtonsText.bots_remove)
bots_kb.add(btn_bt_list)
bots_kb.add(btn_bt_control)
bots_kb.add(btn_bt_log)
bots_kb.add(btn_bt_remove)
bots_kb.add(btn_back)

# Settings keyboard and buttons
settings_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_st_add_notify = KeyboardButton(ButtonsText.settings_notify_add)
btn_st_control_notify = KeyboardButton(ButtonsText.settings_notify_control)
settings_kb.add(btn_st_add_notify)
settings_kb.add(btn_st_control_notify)
settings_kb.add(btn_back)

# Admin menu keyboard and buttons
admin_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_add_admin = KeyboardButton(ButtonsText.admin_add_admin)
btn_del_admin = KeyboardButton(ButtonsText.admin_del_admin)

admin_kb.add(btn_add_admin)
admin_kb.add(btn_del_admin)
admin_kb.add(btn_back)

# User's main menu
user_main_menu_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_umm_botlist = KeyboardButton(ButtonsText.user_bots)
btn_umm_support = KeyboardButton(ButtonsText.support)
user_main_menu_kb.add(btn_umm_botlist)
user_main_menu_kb.add(btn_umm_support)

# User's support menu
user_support_kb = ReplyKeyboardMarkup(resize_keyboard=True)
btn_us_faq = KeyboardButton(ButtonsText.support_faq)
btn_us_write = KeyboardButton(ButtonsText.support_write)
user_support_kb.add(btn_us_faq)
user_support_kb.add(btn_us_write)
user_support_kb.add(btn_back)

# Inline keyboards
# Control bot inline keyboard
running_kb = InlineKeyboardMarkup()
ibtn_stop_bot = InlineKeyboardButton(
    ButtonsText.inl_stop, callback_data='bot_stop'
    )
ibtn_restart_bton = InlineKeyboardButton(
    ButtonsText.inl_restart, callback_data='bot_restart'
)
running_kb.add(ibtn_stop_bot, ibtn_restart_bton)

stopped_kb = InlineKeyboardMarkup()
ibtn_start_bot = InlineKeyboardButton(
    ButtonsText.inl_start, callback_data='bot_start'
    )
stopped_kb.add(ibtn_start_bot)

# Ask for delete notify inline keyboard
ask_del_notify_kb = InlineKeyboardMarkup()
ibtn_yes_delete_notify = InlineKeyboardButton(
    'Да', callback_data='notify_del_yes'
    )
ibtn_no_delete_notify = InlineKeyboardButton(
    'Нет', callback_data='notify_del_no'
    )
ask_del_notify_kb.add(ibtn_yes_delete_notify, ibtn_no_delete_notify)

# Ask for delete bot inlint keyboard
ask_del_bot_kb = InlineKeyboardMarkup()
ibtn_yes_delete_bot = InlineKeyboardButton(
    'Да', callback_data='del_bot_yes'
    )
ibtn_no_delete_bot = InlineKeyboardButton(
    'Нет', callback_data='del_bot_no'
    )
ask_del_bot_kb.add(ibtn_yes_delete_bot, ibtn_no_delete_bot)

# Ask for add user in admins
ask_add_admin_kb = InlineKeyboardMarkup()
ibtn_yes_admin_add = InlineKeyboardButton(
    'Да', callback_data='admin_add_yes'
    )
ibtn_no_admin_add = InlineKeyboardButton(
    'Нет', callback_data='admin_add_no'
    )
ask_add_admin_kb.add(ibtn_yes_admin_add, ibtn_no_admin_add)

# Ask for delete admin
ask_del_admin_kb = InlineKeyboardMarkup()
ibtn_yes_admin_del = InlineKeyboardButton(
    'Да', callback_data='admin_del_yes'
    )
ibtn_no_admin_del = InlineKeyboardButton(
    'Нет', callback_data='admin_del_no'
    )
ask_del_admin_kb.add(ibtn_yes_admin_del, ibtn_no_admin_del)


# Check for valid callback keyboard notify delete
def check_notify_buttons(call_data):
    """Checks for valid button data in inline keyboard for delete notify"""
    if 'notify_del_' in call_data:
        return True
    return False


# Check for valid callback keyboard bot delete
def check_bot_buttons(call_data):
    """Checks for valid button data in inline keyboard for delete notify"""
    if 'del_bot_' in call_data:
        return True
    return False


# Check for valid callback keyboard bot control
def check_bots_control(call_data):
    """Checks for valid callback data of inline keyboard control bots"""
    if call_data[0:4] == 'bot_':
        return True
    return False


# Checks for valid admin additions inline keyboard
def check_admin_add_buttons(call_data):
    """Checks for valid put buttons"""
    if 'admin_add_' in call_data:
        return True
    return False


# Checks for valid admin additions inline keyboard
def check_admin_del_buttons(call_data):
    """Checks for valid put buttons"""
    if 'admin_del_' in call_data:
        return True
    return False


# Keyboard by bots names
def bots_names_kb():
    """Returns bots names like keyboard"""
    botnames = bots_table.get_bots_names()
    botnames_kb = ReplyKeyboardMarkup(resize_keyboard=True)

    for botname in botnames:
        tmp_btn = KeyboardButton(botname[0])
        botnames_kb.add(tmp_btn)

    botnames_kb.add(btn_back)

    return botnames_kb


def bots_names_logs_kb():
    """Returns bots names like keyboard"""
    botnames = bots_table.get_bots_names()
    main_bot = KeyboardButton("Контроллер")
    botnames_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    botnames_kb.add(main_bot)

    for botname in botnames:
        tmp_btn = KeyboardButton(botname[0])
        botnames_kb.add(tmp_btn)

    botnames_kb.add(btn_back)

    return botnames_kb


def check_bots_names_buttons(botname):
    """Checks for valid user's button click"""
    botnames = bots_table.get_bots_names()
    for name in botnames:
        if botname in name:
            return True
    return False


# Keyboard by users names
def users_names_kb(botname=None):
    """Returns keyboard of usernames list"""
    if botname:
        usernames = notify_table.get_userlist_by_botname(botname)
    else:
        usernames = users_table.get_userlist()
    usernames_kb = ReplyKeyboardMarkup(resize_keyboard=True)

    for username in usernames:
        tmp_btn = KeyboardButton(username[0])
        usernames_kb.add(tmp_btn)

    usernames_kb.add(btn_back)

    return usernames_kb


def regular_users_kb():
    """Returns regular users keyboard"""
    userlist_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    users = users_table.get_regular_userlist()

    for user in users:
        tmp_btn = KeyboardButton(user[0])
        userlist_kb.add(tmp_btn)

    userlist_kb.add(btn_back)
    return userlist_kb


def admin_users_kb():
    """Returns admin users keyboard"""
    adminlist_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    admins = users_table.get_admin_userlist()

    for admin in admins:
        tmp_btn = KeyboardButton(admin[0])
        adminlist_kb.add(tmp_btn)

    adminlist_kb.add(btn_back)
    return adminlist_kb
