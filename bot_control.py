"""Main file for telegram bots control bot"""

##############################################
#                                            #
#        Telegram controller bots            #
#        Powered by DigitalMonroe            #
#         Telegram: @ni_nitca                #
#                                            #
##############################################

# -*- coding: utf-8 -*-

import atexit
import threading
import telebot
import config
import menu_and_status
import controller
import lib.database_work
import lib.monitoring as monitoring
import lib.keyboard_bot as keyboard_bot
import lib.telebot_user_state as telebot_user_state

bot = telebot.TeleBot(config.TOKEN)
bots_table = lib.database_work.BotWork()
user_table = lib.database_work.UserWork()
notify_table = lib.database_work.NotifyWork()

users_states = telebot_user_state.UserStates()
users_data = telebot_user_state.UserData()


def back_to_main_menu(msg):
    """Returns user in main menu and drops all data"""
    users_data.drop_data(msg.chat.id)
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.main_menu,
        reply_markup=keyboard_bot.mainmenu_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.main_menu
    )


def back_to_bots_menu(msg):
    """Returns user in main menu and drops all data"""
    users_data.drop_data(msg.chat.id)
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.bots_menu,
        reply_markup=keyboard_bot.bots_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.bots_menu
        )


def back_to_settings_menu(msg):
    """Returns user in main menu and drops all data"""
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.settings,
        reply_markup=keyboard_bot.settings_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.settings
        )


def back_to_admin_menu(msg):
    """Returns user in admin menu"""
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.admin_yes,
        reply_markup=keyboard_bot.admin_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.admin
        )


def back_to_user_menu(msg):
    """Low rules for regular user without admin access"""
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.user_menu,
        reply_markup=keyboard_bot.user_main_menu_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.user_menu
        )


def back_to_user_support_menu(msg):
    """Returns user to support menu"""
    bot.send_message(
        msg.chat.id,
        menu_and_status.SendText.user_support,
        reply_markup=keyboard_bot.user_support_kb
        )
    users_states.update_state(
        msg.chat.id, menu_and_status.Status.user_support
        )


@bot.message_handler(commands=['start'])
def start_func(msg):
    """Adds user's file, adds user data in table and sends in main menu"""
    user_table.add_new_user(msg.chat.id, msg.chat.username)
    is_admin = user_table.admin_level(msg.chat.username)
    if is_admin == 0:
        back_to_user_menu(msg)
        bot.send_message(
            msg.chat.id, 'Ожидайте уведомлений о работе ботов'
            )
    else:
        users_data.add_user_in_data(msg.chat.id)
        back_to_main_menu(msg)


# Main menu
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.main_menu
            )
        )
def main_menu(msg):
    """Main menu logic buttons"""
    if msg.text == keyboard_bot.ButtonsText.add_bot:
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.add_bot_archive
            )
        with open('bot_control.ini', 'r') as bconf:
            bot.send_document(
                chat_id=msg.chat.id,
                data=bconf,
                caption=menu_and_status.SendText.add_bot_archive,
                reply_markup=keyboard_bot.back_kb
                )

    if msg.text == keyboard_bot.ButtonsText.bots:
        back_to_bots_menu(msg)

    if msg.text == keyboard_bot.ButtonsText.settings:
        back_to_settings_menu(msg)

    if msg.text == keyboard_bot.ButtonsText.mailing:
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.mailing
            )
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.mailing,
            reply_markup=keyboard_bot.back_kb
            )

    if msg.text == keyboard_bot.ButtonsText.admin:
        admin_level = user_table.admin_level(msg.chat.username)
        if admin_level < 2:
            bot.send_message(
                msg.chat.id, menu_and_status.SendText.admin_no
                )
        else:
            back_to_admin_menu(msg)


# Addition bot
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.add_bot_archive
            )
        )
@bot.message_handler(content_types=['document'])
def get_new_bot_archive(msg):
    """Gets users's archive with bot and starts it"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_main_menu(msg)
        return

    if msg.document.file_name[-4:] != '.zip':
        bot.send_message(msg.chat.id, 'Принимается только zip архив')
        return

    file_path = bot.get_file(msg.document.file_id).file_path
    userfile = bot.download_file(file_path)
    with open(msg.document.file_name, 'wb') as bot_arch:
        bot_arch.write(userfile)

    botname, check, is_new = controller.extract_and_run(msg.document.file_name)
    if not check:
        bot.send_message(
            msg.chat.id,
            "Для логирования требуется функция main(), "
            "добавьте её и попробуйте ещё раз"
        )
    else:
        if not is_new:
            success_message = "Бот успешно обновлён и перезапущен"
            controller.stop_bot(bot, botname)
        else:
            success_message = "Бот успешно запущен"

        controller.start_bot(bot, botname)

        if controller.running_bot(botname):
            bot.send_message(msg.chat.id, success_message)
            back_to_main_menu(msg)
        else:
            bot.send_message(
                msg.chat.id, 'Не удалось запустить бот: %s' % botname
                )


# Bot control menu
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.bots_menu
            )
        )
def bots_menu(msg):
    """Main menu of bots"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_main_menu(msg)
        return

    if msg.text == keyboard_bot.ButtonsText.bots_list:
        send_text = controller.get_bots_data(msg.chat.username)
        bot.send_message(msg.chat.id, send_text, parse_mode="HTML")

    if msg.text == keyboard_bot.ButtonsText.settings:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.settings,
            reply_markup=keyboard_bot.settings_kb
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.settings
            )

    if msg.text == keyboard_bot.ButtonsText.bots_log:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.bots_log,
            reply_markup=keyboard_bot.bots_names_logs_kb()
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.bots_log
        )

    if msg.text == keyboard_bot.ButtonsText.bots_control:
        botnames_kb = keyboard_bot.bots_names_kb()
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.bots_list_control,
            reply_markup=botnames_kb
        )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.bots_list_control
            )

    if msg.text == keyboard_bot.ButtonsText.bots_remove:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.bots_remove,
            reply_markup=keyboard_bot.bots_names_kb()
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.bots_remove
            )


# Bot control stop, pause and restart
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.bots_list_control
            )
        )
def bots_list_control(msg):
    """Menu for bots list control"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_bots_menu(msg)
        return

    if keyboard_bot.check_bots_names_buttons(msg.text):
        bot_status = bots_table.get_bot_status(msg.text)
        if bot_status == 1:
            status_text = "Бот %s сейчас работает.\n\n"\
                "Вы можете его остановить или перезапустить" % msg.text
            running_kb = keyboard_bot.running_kb
            bot.send_message(
                msg.chat.id, status_text, reply_markup=running_kb
                )

        elif bot_status == 0:
            stopped_kb = keyboard_bot.stopped_kb
            status_text = "Бот %s сейчас остановлен.\n\n"\
                "Вы можете его запустить" % msg.text
            bot.send_message(
                msg.chat.id, status_text, reply_markup=stopped_kb
                )

        users_data.add_data(
            msg.chat.id, menu_and_status.Status.bots_list_control, msg.text
            )


@bot.callback_query_handler(
    func=lambda call: keyboard_bot.check_bots_control(call.data)
    )
def bots_start_stop_restart(call):
    """Inline keyboards for bots control"""
    botname = users_data.get_all_data_by_id(
        call.message.chat.id
        )[menu_and_status.Status.bots_list_control]
    if call.data == 'bot_stop':
        controller.stop_bot(bot, botname)
        bot.edit_message_text(
            text="%s успешно остановлен" % botname,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )

    if call.data == 'bot_start':
        controller.start_bot(bot, botname)
        bot.edit_message_text(
            text="%s успешно запущен" % botname,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )

    if call.data == 'bot_restart':
        controller.stop_bot(bot, botname)
        controller.start_bot(bot, botname)
        bot.edit_message_text(
            text="%s успешно перезапущен" % botname,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )


# Bots log
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.bots_log
            )
        )
def logs_bots_menu(msg):
    """User's choise bot by name for het log"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_bots_menu(msg)
        return

    if keyboard_bot.check_bots_names_buttons(msg.text) or\
            msg.text == 'Контроллер':
        if msg.text == 'Контроллер':
            msg.text = 'controller'
        bot.send_message(
            msg.chat.id,
            "Введите количество строк, которые хотите получить из журнала:",
            reply_markup=keyboard_bot.back_kb
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.bots_log_lines
            )
        users_data.add_data(
            msg.chat.id, menu_and_status.Status.bots_log, msg.text
            )


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.bots_log_lines
        )
    )
def get_log_lines(msg):
    """Gets user's count lines for log"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_bots_menu(msg)
        return

    text = msg.text
    if text.isdigit():
        linesnum = int(text)
        if linesnum <= 0:
            bot.send_message(
                msg.chat.id, "Число строк должно быть больше нуля"
                )
        else:
            botname = users_data.get_all_data_by_id(msg.chat.id)
            botname = botname[menu_and_status.Status.bots_log]
            logtext = controller.get_loglines(botname, linesnum)
            if len(logtext) == 0:
                bot.send_message(msg.chat.id, 'Лог файл этого бота пуст')
            else:
                bot.send_message(msg.chat.id, logtext)


# Bot remove
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.bots_remove
            )
    )
def remove_bot(msg):
    """Removes bot from controller by botname"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_bots_menu(msg)
        return

    check = keyboard_bot.check_bots_names_buttons(msg.text)
    if check:
        botdel_message = "Удалить бот <b>%s</b> ?" % msg.text
        bot.send_message(
            msg.chat.id,
            botdel_message,
            reply_markup=keyboard_bot.ask_del_bot_kb,
            parse_mode='HTML'
            )
        users_data.add_data(
            msg.chat.id,
            menu_and_status.Status.bots_remove,
            msg.text
            )


@bot.callback_query_handler(
    func=lambda call: keyboard_bot.check_bot_buttons(call.data)
    )
def ask_for_delete_bot(call):
    """Iline keyboard ask for delete bot"""
    botname = users_data.get_all_data_by_id(
        call.message.chat.id
        )[menu_and_status.Status.bots_remove]

    if call.data == 'del_bot_yes':
        fulltext = "Бот <b>%s</b> успешно удалён" % botname
        bot.edit_message_text(
            text=fulltext,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            parse_mode='HTML'
            )
        controller.delete_bot(bot, botname)

    elif call.data == 'del_bot_no':
        fulltext = "Бот <b>%s</b> успешно сохранён" % botname
        bot.edit_message_text(
            text=fulltext,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            parse_mode='HTML'
            )
    back_to_bots_menu(call.message)


# Menu settings
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.settings
            )
        )
def settings_menu(msg):
    """Settings main menu logic"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_main_menu(msg)
        return

    if msg.text == keyboard_bot.ButtonsText.settings_notify_add:
        usernames_kb = keyboard_bot.users_names_kb()
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.settings_notify_user,
            reply_markup=usernames_kb
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.settings_add_notify_user
            )

    if msg.text == keyboard_bot.ButtonsText.settings_notify_control:
        botlist_kb = keyboard_bot.bots_names_kb()
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.settings_notify_bot,
            reply_markup=botlist_kb
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.settings_control_notify_bot
            )


# Add new notify
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.settings_add_notify_user
            )
        )
def settings_add_notify_user(msg):
    """Checks for valid button of usernames"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_settings_menu(msg)
        return

    usernames = user_table.get_userlist()
    for username in usernames:
        if msg.text in username:
            bot.send_message(
                msg.chat.id,
                menu_and_status.SendText.settings_notify_bot,
                reply_markup=keyboard_bot.bots_names_kb()
                )
            users_states.update_state(
                msg.chat.id, menu_and_status.Status.settings_add_notify_bot
                )
            users_data.add_data(
                msg.chat.id,
                menu_and_status.Status.settings_add_notify_user,
                msg.text
                )
            break


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.settings_add_notify_bot
            )
        )
def settings_add_notify_bot(msg):
    """Checks for valid button of usernames"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_settings_menu(msg)
        return

    botnames = bots_table.get_bots_names()
    for botname in botnames:
        if msg.text in botname:
            username = users_data.get_all_data_by_id(
                msg.chat.id
                )[menu_and_status.Status.settings_add_notify_user]
            user_id = user_table.get_user_id(username)
            if notify_table.check_for_double(user_id, msg.text):
                final_notify_text =\
                    "Оповещение о работе бота <b>%s</b> для пользователя"\
                    " <b>%s</b> успешно добавлено" % (msg.text, username)
                bot.send_message(
                    msg.chat.id, final_notify_text, parse_mode='HTML'
                    )
                notify_table.update_bot_for_user(user_id, username, msg.text)
            else:
                final_notify_text =\
                    "У пользователя <b>%s</b> уже настроено оповещение для"\
                    " бота <b>%s</b>" % (username, msg.text)
                bot.send_message(
                    msg.chat.id, final_notify_text, parse_mode='HTML'
                )
            back_to_settings_menu(msg)


# Control notifyes
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.settings_control_notify_bot
            )
        )
def control_notify_bots(msg):
    """User's choise from botlist"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_settings_menu(msg)
        return

    botnames = bots_table.get_bots_names()
    for botname in botnames:
        if msg.text in botname:
            users_kb = keyboard_bot.users_names_kb(msg.text)
            users_data.add_data(
                msg.chat.id,
                menu_and_status.Status.settings_control_notify_bot,
                msg.text
                )
            users_states.update_state(
                msg.chat.id,
                menu_and_status.Status.settings_control_notify_user
                )
            bot.send_message(
                msg.chat.id, menu_and_status.SendText.settings_notify_user,
                reply_markup=users_kb
                )


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.settings_control_notify_user
            )
        )
def control_notify_users(msg):
    """User's choise from botlist"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_settings_menu(msg)
        return

    botname = users_data.get_all_data_by_id(
        msg.chat.id
        )[menu_and_status.Status.settings_control_notify_bot]
    usernames = notify_table.get_userlist_by_botname(botname)
    for username in usernames:
        if msg.text in username:
            fullmessage = "Удалить напоминание о работе бота <b>%s</b> для "\
                "пользователя <b>%s</b>" % (msg.text, botname)

            bot.send_message(
                msg.chat.id,
                fullmessage,
                parse_mode='HTML',
                reply_markup=keyboard_bot.ask_del_notify_kb
                )
            users_data.add_data(
                msg.chat.id,
                menu_and_status.Status.settings_control_notify_user,
                msg.text
                )


@bot.callback_query_handler(
    func=lambda call: keyboard_bot.check_notify_buttons(call.data)
    )
def ask_for_delete_notify(call):
    """Butons logic for delete notification"""
    notify_data = users_data.get_all_data_by_id(call.message.chat.id)
    botname = notify_data[menu_and_status.Status.settings_control_notify_bot]
    username = notify_data[menu_and_status.Status.settings_control_notify_user]

    if call.data == 'notify_del_yes':
        final_message = "Оповещение о работе бота <b>%s</b> "\
            "для пользователя <b>%s</b> успешно <b>удалено</b>" %\
            (botname, username)
        notify_table.delete_bot_for_user(username, botname)
        bot.edit_message_text(
            text=final_message,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            parse_mode='HTML'
            )
    if call.data == 'notify_del_no':
        final_message = "Оповещение о работе бота <b>%s</b> "\
            "для пользователя <b>%s сохранено</b>" % (botname, username)
        bot.edit_message_text(
            text=final_message,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            parse_mode='HTML'
            )
    back_to_settings_menu(call.message)


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.mailing
            )
        )
def mailing(msg):
    """Mailings admin's message for all users"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_main_menu(msg)
        return

    users_id = user_table.get_all_users_id()

    for user_id in users_id:
        bot.send_message(user_id[0], msg.text)
    back_to_main_menu(msg)


# Admin menu
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.admin
            )
        )
def admin_menu(msg):
    """Admin main menu"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_main_menu(msg)
        return

    if msg.text == keyboard_bot.ButtonsText.admin_add_admin:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.admin_choise,
            reply_markup=keyboard_bot.regular_users_kb()
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.admin_add_admin
            )
    if msg.text == keyboard_bot.ButtonsText.admin_del_admin:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.admin_choise,
            reply_markup=keyboard_bot.admin_users_kb()
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.admin_del_admin
            )


# Add new admin
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.admin_add_admin
            )
        )
def add_new_admin(msg):
    """Buttons and logic for add new admin"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_admin_menu(msg)
        return

    users = user_table.get_regular_userlist()
    for user in users:
        if msg.text in user:
            fulltext =\
                "Добавить пользователю <b>%s</b> права администратора?" %\
                msg.text
            bot.send_message(
                msg.chat.id,
                fulltext,
                parse_mode='HTML',
                reply_markup=keyboard_bot.ask_add_admin_kb
                )
            users_data.add_data(
                msg.chat.id, menu_and_status.Status.admin_add_admin, msg.text)
            break


@bot.callback_query_handler(
    func=lambda call: keyboard_bot.check_admin_add_buttons(call.data)
    )
def inline_add_new_admin(call):
    """Logic for inline keyboard in addition new admin"""
    username = users_data.get_all_data_by_id(
        call.message.chat.id
        )[menu_and_status.Status.admin_add_admin]
    if call.data == 'admin_add_yes':

        rules_message = "Вам предоставлены права администратора.\n"\
            "Для получения контроля Вам нужно перезапустить бот командой:\n"\
            "/start"
        user_id = user_table.increase_rules_and_return_id(username)

        bot.send_message(user_id, rules_message)
        bot.edit_message_text(
            text='Права администратора добавлены %s' % username,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )

    if call.data == 'admin_add_no':
        bot.edit_message_text(
            text='У пользователя %s сохранены текущие права' % username,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )
    back_to_admin_menu(call.message)


# Delete admin
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.admin_del_admin
            )
        )
def delete_admin(msg):
    """Buttons and logic for delete admin"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_admin_menu(msg)
        return

    admins = user_table.get_admin_userlist()
    for admin in admins:
        if msg.text in admin:
            fulltext =\
                "Удалить пользователю <b>%s</b> права администратора?" %\
                msg.text
            bot.send_message(
                msg.chat.id,
                fulltext,
                parse_mode='HTML',
                reply_markup=keyboard_bot.ask_del_admin_kb
                )
            users_data.add_data(
                msg.chat.id, menu_and_status.Status.admin_add_admin, msg.text)
            break


@bot.callback_query_handler(
    func=lambda call: keyboard_bot.check_admin_del_buttons(call.data)
    )
def inline_delete_admin(call):
    """Logic for inline keyboard in addition new admin"""
    username = users_data.get_all_data_by_id(
        call.message.chat.id
        )[menu_and_status.Status.admin_add_admin]
    if call.data == 'admin_del_yes':
        rules_message = "Уведомляем Вас о понижении прав доступа"
        user_id = user_table.reduce_rules_and_return_id(username)

        bot.send_message(
            user_id,
            rules_message,
            reply_markup=telebot.types.ReplyKeyboardRemove()
            )
        bot.edit_message_text(
            text='Права администратора удалены у %s' % username,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )
        users_states.update_state(
            user_id, menu_and_status.Status.user_menu
            )

    if call.data == 'admin_del_no':
        bot.edit_message_text(
            text='У пользователя %s сохранены текущие права' % username,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )
    back_to_admin_menu(call.message)


# User menu
@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.user_menu
            )
        )
def user_main_menu(msg):
    """Main menu for users without admin rules"""
    if msg.text == keyboard_bot.ButtonsText.user_bots:
        status_bots = controller.get_bots_data(msg.chat.username)
        bot.send_message(msg.chat.id, status_bots, parse_mode='HTML')
    if msg.text == keyboard_bot.ButtonsText.support:
        back_to_user_support_menu(msg)


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.user_support
            )
        )
def user_support_menu(msg):
    """Main menu for support user"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_user_menu(msg)
        return

    if msg.text == keyboard_bot.ButtonsText.support_faq:
        bot.send_message(
            msg.chat.id, menu_and_status.SendText.user_faq
            )
        with open('faq_text', 'r') as faq_text:
            bot.send_message(
                msg.chat.id,
                faq_text.read()
                )

    if msg.text == keyboard_bot.ButtonsText.support_write:
        bot.send_message(
            msg.chat.id,
            menu_and_status.SendText.user_write,
            reply_markup=keyboard_bot.back_kb
            )
        users_states.update_state(
            msg.chat.id, menu_and_status.Status.user_write
            )


@bot.message_handler(
    func=lambda msg:
        users_states.is_current_state(
            msg.chat.id, menu_and_status.Status.user_write
            )
        )
def user_write_to_admin(msg):
    """Sends user's message to admins"""
    if msg.text == keyboard_bot.ButtonsText.back:
        back_to_user_support_menu(msg)
        return

    if len(msg.text) < 10:
        bot.send_message(
            msg.chat.id, 'Сообщение должно быть не менее 10 символов'
            )
        bot.send_message(msg.chat.id, menu_and_status.SendText.user_write)
    else:
        final_message = controller.user_support_message_and_data(
            msg.chat.username, msg.text
            )
        admins = user_table.get_superadmins_ids()
        for admin in admins:
            bot.send_message(admin[0], final_message, parse_mode='HTML')
        bot.send_message(
            msg.chat.id, 'Ваше обращение отправлено. Ожидайте ответа'
            )
        back_to_user_support_menu(msg)


def main():
    # Kill all bots on exit

    atexit.register(controller.stop_all_bots, bot=bot)

    controller.start_all_bots(bot)

    monitor_thread = threading.Thread(
        target=monitoring.monitoring_for_bots_working,
        args=[bot]
        )
    monitor_thread.start()
    try:
        bot.infinity_polling(timeout=120)
    except KeyboardInterrupt:
        return
    except Exception:
        monitor_thread.join()
        main()
        monitor_thread.join()


if __name__ == '__main__':
    main()
