"""Run, stop, reload and unpack bots for telegram bot controller bots"""

# -*- coding: utf-8 -*-

import os
import shutil
import signal
import zipfile
import time
import subprocess
import configparser
import loguru
import lib.database_work

DIR = '/home/d0205toff/code/python/job/bot_control'
PATH_TO_CONTROLLER = os.path.curdir
CONFIG_NAME = 'bot_control.ini'
bot_table = lib.database_work.BotWork()
notify_table = lib.database_work.NotifyWork()

loguru.logger.add(
        'logs/controller.log',
        format='DATE: {time:: DD-MM-YYYY} TIME: {time:: HH:mm:ss}"\
        " LEVEL: {level} {message}',
        rotation="10 KB",
        compression="zip"
        )


def _add_loger(scriptname, scriptpath):
    """Addition logger in code file of bot"""
    logger_text = \
        "\n\nloguru.logger.add(\n"\
        "\t'logs/%s.log',\n"\
        "\tformat='DATE: {time:: DD-MM-YYYY} TIME: {time:: HH:mm:ss}'\\\n"\
        "\t' LEVEL: {level}\\n{message}\\n',\n"\
        "\trotation='10 KB',\n"\
        "\tcompression='zip'\n"\
        ")\n\n\n"\
        "@loguru.logger.catch" % scriptname

    with open(scriptpath, 'r', encoding='utf-8') as scpf:
        text_for_find_logger = scpf.read()
        if logger_text in text_for_find_logger:
            return

    with open(scriptpath, 'r', encoding='utf-8') as scpf:
        counter = 0
        lines = scpf.readlines()
        if 'import loguru' not in lines:
            is_import_found = False
            for line in lines:
                if is_import_found:
                    if 'import' not in line:
                        lines.insert(counter, 'import loguru')
                        break

                if 'import' in line and not is_import_found:
                    is_import_found = True
                counter += 1

        try:
            counter = lines.index('def main():\n')
        except ValueError:
            return False
        lines.insert(counter - 1, logger_text)

    with open(scriptpath, 'w') as scpf:
        for line in lines:
            scpf.write(line)
    return True


def get_loglines(botname, lines):
    """Gets textlines from log file"""
    path = './logs/' + botname + '.log'
    try:
        with open(path, 'r') as logfile:
            loglines = logfile.readlines()
    except FileNotFoundError:
        return ''

    linecount = len(loglines) - 1
    i = linecount
    logtext = ''
    while i > linecount - lines and i >= 0:
        logtext += loglines[i]
        i -= 1
    return logtext


def _get_bot_data_from_config_or_none(path):
    """Returns botname and path to script"""
    path = "%s/%s" % (path, CONFIG_NAME)

    if os.path.exists(path):
        config = configparser.ConfigParser()
        config.read(path)
    else:
        return False, 'Файл конфигураций не найден', None

    try:
        name = config['Bot']['name']
        script = config['Bot']['script']
        username = config['Bot']['username']
    except KeyError as e_str:
        return False, 'Не найден ключ: %s' % e_str, None

    return name, script, username


def extract_and_run(archive):
    """Extracts zip archive, moves it in DIR and runs bot"""
    loguru.logger.info("ADDITION %s" % archive)

    path = "%s/%s" % (DIR, archive[0:-4])

    bot_archive = zipfile.ZipFile(archive)
    bot_archive.extractall(DIR)
    bot_archive.close()

    os.remove(archive)

    botname, scriptname, username = _get_bot_data_from_config_or_none(path)

    if not botname:
        loguru.warning("Can't get botname %s" % archive)
        return botname, scriptname, None

    scriptpath = "%s/%s" % (path, scriptname)
    check = _add_loger(scriptname, scriptpath)
    if check:
        is_new = bot_table.check_for_new_bot(botname)
        bot_table.add_bot(botname, scriptpath, path, username)
    else:
        is_new = False
        shutil.rmtree(path)

    return botname, check, is_new


def start_bot(bot, botname):
    """Starts bot by bot name and updates bot status in database"""
    loguru.logger.info("STARTED %s" % botname)
    logname = "logs/%s.log" % botname
    logname = open(logname, 'w+')

    bot_data = bot_table.get_all_data_of_bot(botname)
    path_bot = bot_data[3]

    new_bot = subprocess.Popen(
        ['python', path_bot],
        cwd=bot_data[4],
        stdout=logname,
        stderr=logname
        )

    bot_table.update_bot_status(botname, 1, new_bot.pid)
    if new_bot.returncode is None:
        users_id = notify_table.get_users_id_by_botname(botname)
        stop_message = "Уведомляем Вас, что бот '%s' успешно запущен.\n"\
            "Можете продолжить работу.\n"\
            "Приятного пользования" % botname
        for user_id in users_id:
            bot.send_message(user_id[0], stop_message)

    return new_bot


def stop_bot(bot, botname):
    """Stops bot by botname and send notification for users"""
    loguru.logger.info("STOPING %s" % botname)

    try:
        process_pid = bot_table.get_process_bot_pid(botname)[0]
        if process_pid != 0:
            os.kill(process_pid, signal.SIGTERM)
        bot_table.update_bot_status(botname, 0)

        users_id = notify_table.get_users_id_by_botname(botname)
        stop_message = "Бот '%s' В данный момент отключён.\n"\
            "Мы постараемся это исправить в ближайшее время.\n"\
            "Благодарим за понимание и терпение" % botname
        for user_id in users_id:
            bot.send_message(user_id[0], stop_message)

    except ProcessLookupError as e:
        loguru.logger.exception(f"Can't kill process: {e}")


def get_bots_data(username):
    """Returns list of strings with bots data"""
    diff_seconds = bot_table.get_bots_time_diffirence(username)
    bots_data = bot_table.get_bots_data(username)
    if len(bots_data) == 0:
        return "В базе пока нет ботов"

    final_message = ""

    for seconds, data in zip(diff_seconds, bots_data):
        data = data[0]
        if data[2] == 0:
            timelaunch = '  Дней 00, 00:00:00'
            status = "🔴"
        else:
            tmp_time = time.gmtime(seconds)
            timelaunch = time.strftime("Дней %d, %H:%M:%S", tmp_time)
            status = "🟢"
        final_message += "<b>Имя бота:</b>           %s\n"\
            "<b>Username:</b>          %s\n"\
            "<b>Время работы:</b> %s\n<b>Статус бота:</b>       %s\n"\
            "<b>-------------------------------------</b>\n" %\
            (data[0], data[1], timelaunch, status)

    return final_message


def delete_bot(bot, botname):
    """Deletees bot from table and from server"""
    stop_bot(bot, botname)
    path = bot_table.get_botpath_by_botname(botname)
    shutil.rmtree(path)
    bot_table.remove_bot_by_botname(botname)
    notify_table.remove_all_notify_of_botname(botname)


def start_all_bots(bot):
    """Starts all bots"""
    botnames = bot_table.get_bots_names()

    for botname in botnames:
        start_bot(bot, botname[0])


def stop_all_bots(bot):
    """Stops all bots"""
    botnames = bot_table.get_active_botnames()

    for botname in botnames:
        stop_bot(bot, botname[0])


def user_support_message_and_data(username, message):
    """Returns user's message plus time, data and username"""
    today = time.time()
    today = time.strftime("<b>Дата</b>: %d.%m\n<b>Время</b>: %H:%M")

    final_message = "<b>Пользователь</b>: @%s\n%s\n<b>Обращение</b>:\n%s" %\
        (username, today, message)

    return final_message


def running_bot(botname):
    """Checks for working bot by botname"""
    bot_pid = bot_table.get_process_bot_pid(botname)[0]
    processes = os.popen('ps ax').read()
    if processes.find(str(bot_pid)) == -1 or bot_pid == 0:
        return False
    return True
