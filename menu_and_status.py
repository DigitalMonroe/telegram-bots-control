"""All text data of menu messagies and statuses users"""

# -*- coding: utf-8 -*-


class Status:
    """Text data for work with users status"""
    user_menu = 'user_menu'
    user_support = 'user_support'
    user_faq = 'user_faq'
    user_write = 'user_write'

    main_menu = 'main_menu'

    add_bot_name = 'add_bot_name'
    add_bot_archive = 'add_bot_archive'

    bots_menu = 'bots_menu'
    bots_list = 'bots_list'
    bots_log = 'bots_log'
    bots_log_lines = 'bots_log_lines'
    bots_remove = 'bots_remove'
    bots_list_control = 'bots_list_control'

    settings = 'settings'
    settings_add_notify_user = 'settings_add_notify_user'
    settings_add_notify_bot = 'settings_add_notify_bot'

    settings_control_notify_bot = 'settings_control_notify_bot'
    settings_control_notify_user = 'settings_control_notify_user'

    mailing = 'mailing'

    admin = 'admin'
    admin_add_admin = 'admin_add_admin'
    admin_del_admin = 'admin_del_admin'


class SendText:
    """Text for send users from menu"""
    user_menu = 'Главное меню'
    user_support = 'Меню помощи'
    user_faq = 'Вопрос-Ответ'
    user_write = 'Напишите Ваше обращение администрации(Не менее 10 символов):'
    main_menu = 'Главное меню'

    add_bot_name = 'Введите имя бота:'
    add_bot_archive = '1. Бот принимается только в формате zip архива\n\n'\
        '2. У бота должен быть файл конфигурации \'bot_control.ini\''\
        ' Вы можете скачать этот пример\n\n'\
        '3. В файле конфигурации 3 поля: name, script, username:\n'\
        ' ·name - имя для записи в сам контролер;\n'\
        ' ·script - файла запуска бота;\n'\
        ' ·username - username из телеграм бота;'
    bots_menu = 'Меню ботов'
    bots_remove = 'Выберите бота:'
    bots_log = 'Выберите журнал по имени бота:'
    bots_list_control = 'Выберите бота по имени:'

    settings = 'Меню уведомлений'
    settings_notify_user = 'Выберите пользователя:'
    settings_notify_bot = 'Выберите бот:'

    mailing = 'Введите рассылаемое сообщение:'

    admin_yes = 'Меню администратора'
    admin_no = 'Вы не являетесь администратором'
    admin_choise = 'Выберите пользователя:'
